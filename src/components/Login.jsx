import React, { useState } from 'react';
import styled, { css, ThemeProvider } from 'styled-components';

const Link = styled.a`
  display: flex;
  align-items: center;
  padding: 5px 10px;
  background: papayawhip;
  color: palevioletred;
`;
const Icon = styled.svg`
  flex: none;
  transition: fill 0.25s;
  width: 48px;
  height: 48px;

  ${Link}:hover {
    fill: yellow;
  }
`;
const Label = styled.span`
  display: flex;
  align-items: center;
  line-height: 1.2;
  &::before {
    content: '◀';
    margin: 0 10px;
  }
`;


const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    function handleInputChange({ target: { name, value } }) {
        switch (name) {
            case 'username': setUsername(value);
                break;
            case 'password': setPassword(value);
                break;
        }
    }
    function onSubmit(e) {
        e.preventDefault();
        fetch('http://localhost:3000/api/login', {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: { 'Content-type': 'application/json' }
        }).then(() => {
            window.location.href = 'http://localhost:3000';
        })
            .catch(err => {
                console.log('login fail', err);
            });
    }
    return (
        <form onSubmit={onSubmit}>
            <h1>Login Below!</h1>
            <Link href="#">
                <Icon viewBox="0 0 20 20">
                    <path d="M10 15h8c1 0 2-1 2-2V3c0-1-1-2-2-2H2C1 1 0 2 0 3v10c0 1 1 2 2 2h4v4l4-4zM5 7h2v2H5V7zm4 0h2v2H9V7zm4 0h2v2h-2V7z" />
                </Icon>
                <Label>Hovering my parent changes my style!</Label>
            </Link>
            <input
                type="text"
                name="username"
                placeholder="Enter username"
                value={username}
                onChange={handleInputChange}
                required
            />
            <input
                type="password"
                name="password"
                placeholder="Enter password"
                value={password}
                onChange={handleInputChange}
                required
            />
            <input type="submit" value="Submit" />
        </form>
    );
}

export default Login;