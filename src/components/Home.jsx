import React, { useState, useEffect } from 'react';
import WidthTool from './WidthTool.jsx';
class Cat extends React.Component {
    render() {
        const mouse = this.props.mouse;
        return (
            <img src="/cat.jpg" style={{
                position: 'absolute', left: mouse.x, top:
                    mouse.y
            }} />
        );
    }
}
const Home = () => {
    const [userList, setUserList] = useState();
    const [isLoading, setIsLoading] = useState(false);
    function callAPI() {
        console.log('call api');
        setIsLoading(true);
        fetch("http://localhost:3000/api/users")
            .then(res => res.json())
            .then(res => {
                console.log(res);
                setUserList(res);
                setIsLoading(false);
            })
            .catch(err => {
                console.log(err);
            });
    }
    function deleteUser(username) {
        fetch(`http://localhost:3000/api/users/${username}`, {
            method: 'DELETE'
        }).then(response => {
            callAPI();
        });
    }
    function handleEventName(event) {
        console.log('event.name 可以集中處理event', event.type);
    }
    useEffect(() => {
        callAPI();
    }, []
    );
    return (
        <>
            {isLoading ? (
                <div>Loading...</div>
            ) : (
                    <ul className="userlist" >
                        <li>
                            <WidthTool render={mouse => (
                                <Cat mouse={mouse} />
                            )} />
                        </li>
                        {userList && userList.length > 0 ? userList.map(
                            (user, index) =>
                                <li className="userlist-item" key={index}>
                                    <span className="userlist-username" >{user.username}</span>
                                    <span className="userlist-email" >{user.email}</span>
                                    <button className="userlist-delete" onMouseEnter={handleEventName} onClick={() => deleteUser(user.id)} >delete</button>
                                </li>
                        ) : 'Please Login'}
                    </ul>
                )}
        </>
    );
}

export default Home;
